r = 'r'
t = 't'
e = 'e'
def filterChar():
    words = {}
    with open('words_alpha.txt', 'r') as f:
        for word in f.xreadlines():

            # only r word
            if r in word and not t in word and not e in word:
                words.setdefault('only_r_words', []).append(word)
                continue

            # only e word
            if not r in word and not t in word and e in word:
                words.setdefault('only_e_words', []).append(word)
                continue

            # only t word
            if not r in word and t in word and not e in word:
                words.setdefault('only_t_words', []).append(word)
                continue

            # r and t word
            if r in word and t in word and not e in word:
                words.setdefault('r_and_t_words', []).append(word)
                continue

            # r and e word
            if r in word and not t in word and e in word:
                words.setdefault('r_and_e_words', []).append(word)
                continue

            # e and t word
            if not r in word and t in word and e in word:
                words.setdefault('e_and_t_words', []).append(word)
                continue

            # r and e and t word
            if r in word and t in word and e in word:
                words.setdefault('r_and_e_and_t_words', []).append(word)
                continue

            # other words
            words.setdefault('other_words', []).append(word)

    print('Writing in files')

    for word_types, items in words.items():
        with open(word_types, 'w') as f:
            f.writelines(items)
