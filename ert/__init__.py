STAGE_NAME = [
    'only_r_words',
    'only_e_words',
    'only_t_words',

    'e_and_t_words',
    'r_and_e_words',
    'r_and_t_words',

    'r_and_e_and_t_words',
    'other_words'
]
