#!/usr/bin/python

from window import Window
from user import Batman, select_user
from time import sleep, time
from color import color

class Play(object):
    def __init__(self, newuser, keyset, progress, linelength):
        self.user = Batman(**select_user(newuser, keyset, linelength))

        self.progress = progress
        self.screen = Window((100, 20))

    def line(self, string='Test String', linenumber=0):
        self.screen.move(0, linenumber)
        self.screen.write(string, 'yet')
        self.screen.move(0, linenumber)
        currentChrIndex = 0
        touchE = len(string)        #Diksha@tinder flushed that into my memory
        for chr in string:
            _time = self.screen.getch(ord(chr))
            if _time:
                currentChrIndex += 1
                self.screen.write(chr, 'done')
                self.user.update(1, _time, 0)
                if self.progress:
                    self.screen.update_time(round(self.user.kps, 3), (currentChrIndex, linenumber))
                if currentChrIndex == touchE:
                    return True
            else:
                return False

        self.screen.move(0, linenumber+5)
        self.screen.write('done', 'yet')
        self.screen.move(currentChrIndex, linenumber)
        return True

    def start_test(self, duration):
        try:
            counter = 0
            while duration>0:
                string = self.user.get_string()
                if not string:
                    break
                zero = time()
                status = self.line(string, linenumber=counter)
                gut = time() - zero
                self.user.update(0, 0, 1)
                if not status:
                    self.screen.move(0, 0)
                    self.screen.write("\n\nBackSpace?!, Nobody presses Backspace!\nYou are a looser\nYour KPS is %s"%(self.user.kps), 'done')
                    sleep(5)
                    break
                counter += 1
                if counter >= 20:
                    self.screen.clear()
                    counter = 0
                duration -= gut
        except Exception as e:
            self.screen.close()
            print(color('Error occured\nError: {0.message}', 'FAIL').format(e))
        except KeyboardInterrupt:
            self.screen.close()
            print('\nQuiting...\nCTRL+C pressed.')
        print(color('\nSaving User Data!', 'OKGREEN'))
        self.user.save()
