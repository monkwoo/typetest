#!/usr/bin/python
from os.path import abspath, isfile
from re import match

class Organic(object):
    def __init__(self, START='DATA', END='', db="db"):
        _path = abspath(db)
        if not isfile(_path):
            with open(_path, 'w') as f:
                f.close()
        self._stream = open(abspath(db), 'r+')

    def seek_memory(self, line):
        self._stream.seek(0)
        _line = self._stream.readline()
        while _line and not match(line, _line):
            _line = self._stream.readline()
            continue
        if _line.startswith(line):
            count = len(_line)
            self._stream.seek(-count, 1)
            return count-1      #Remove newline character

    def read(self, matchPtrn):
        if matchPtrn and self.seek_memory(matchPtrn):
            return self._stream.readline()[:-1]

    def delete(self, matchPtrn):
        _count = self.seek_memory(matchPtrn)
        if _count:
            self._stream.write(";"*_count+"\n")
            self._stream.flush()

    def write(self, string):
        self._stream.seek(0, 2)
        self._stream.write("%s\n"%string)
        self._stream.flush()
