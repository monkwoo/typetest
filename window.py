#!/usr/bin/python
import curses, time, os

class Window(object):
    def __init__(self, (width, height)):
        __tty_x__, __tty_y__ = (lambda e: (int(e[1]), int(e[0])))(os.popen('stty size', 'r').read().split())
        ignore = curses.initscr()
        curses.cbreak()
        curses.noecho()
        self.scr = curses.newwin(min(height, __tty_y__), min(width, __tty_x__), 0, 0)
        self.scr.keypad(1)
        self.maxxy = (lambda (a,b): (b,a))(self.scr.getmaxyx())
        self.TIME_ADDRESS = (self.maxxy[0]-7, 0)

    def write(self, string, mode):
        self.scr.addstr(string, {'done': curses.A_DIM, 'yet': curses.A_STANDOUT}[mode])
        self.scr.refresh()

    def move(self, x, y):
        self.scr.move(min(y, (self.maxxy[1]-1)),x)

    def getch(self, ch):
        zero = time.time()
        _ = self.scr.getch()
        while _ != ch:
            curses.beep()
            if _ == curses.KEY_BACKSPACE:
                self.close()
                return False
            _ = self.scr.getch()
        return time.time() - zero

    def update_time(self, t, move_to):
        self.move(*(self.TIME_ADDRESS))
        self.write(str(t), 'yet')
        self.move(*move_to)

    def close(self):
        curses.echo()
        curses.nocbreak()
        self.scr.keypad(0)
        curses.endwin()

    def clear(self):
        # self.scr.clearok(1)
        self.scr.erase()
        self.scr.refresh()
