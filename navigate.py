#!/usr/bin/python

import webbrowser, sys

toUrl = lambda url, comment='': [sys.stdout.write(comment), webbrowser.open(url), sys.exit(0), sys.stdscr.write('\n')]
