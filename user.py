#!/usr/bin/python
from random import choice
from organic import Organic
from os.path import basename, isfile
from re import match
from color import color
from sys import exit

KEYSET = [
    ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~',
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
    ' !"#$%&\'()*+,-./0123456789:;<=>?@[\\]^_`{|}~'
]

class OrganicForTypeTest(Organic):
    def __init__(self, schema):
        Organic.__init__(self)
        self.schema = schema

    def write_user(self, _data):
        ##  Find user and delete if found
        ##  Create New Line with data
        ##  Write Line
        self.delete(_data['name'])
        self.write("|".join([ str(_data[k['key']]) for k in self.schema ]))

    def get_user(self, givenUser):
        # Find user with name
        _line = self.read(givenUser['name'])
        if _line:
            _line = _line.split("|")
            user = {}
            counter = 0
            for each in self.schema:
                user.setdefault(each['key'], each['type'](_line[counter]))
                counter += 1
            return user
        else:
            return givenUser

def parseKeyset(value):
    try:
        return int(value)
    except ValueError:
        return value

this = OrganicForTypeTest(
    [
        { 'key': 'name', 'type': str },
        { 'key': 'age', 'type': int },
        { 'key': 'keys', 'type': int },
        { 'key': 'time', 'type': float },
        { 'key': 'strings', 'type': int },
        { 'key': 'keyset', 'type': parseKeyset },
        { 'key': 'linelength', 'type': int },
    ]
)

def get_keyset_seek(cached, STAGE_NAME):
    _ = match(r'([^:]+):?([0-9]+)?', str(cached))
    if _:
        (_keyset, _seek) = _.groups()
        if _seek:
            _seek = int(_seek)
        else:
            _seek = 0

        if not _keyset in STAGE_NAME:
            _keyset = STAGE_NAME[0]
            _seek = 0
    return _keyset, _seek

class Batman(object):
    def __init__(self, **kwargs):

        user = this.get_user(kwargs)
        assert [setattr(self, k, v) for (k,v) in user.items()]

        if kwargs['keyset'] == 3:
            from ert import STAGE_NAME
            (_keyset, _seek) = get_keyset_seek(user['keyset'], STAGE_NAME)

            _f = open('ert/{0}'.format(_keyset), 'r')
            _f.seek(_seek)
            setattr(self, 'keyset', _f)

    def __getattr__(self, key):
        if key=='kps':
            return float(self.keys)/float(self.time)

    def update(self, keys, time, count=1):
        self.strings += count
        self.keys += keys
        self.time += time

    def get_string(self):
        if type(self.keyset) is file:
            _string = self.keyset.readline().strip() #We don't need new line char
            if not _string:
                self.keyset.close()

                from ert import STAGE_NAME
                _current_index = STAGE_NAME.index(self.keyset.name)
                if _current_index+1 == len(STAGE_NAME):
                    return False

                _f = open('ert/{0}'.format(STAGE_NAME[_current_index+1]), 'r')
                _f.seek(0)
                setattr(self, 'keyset', _f)

                _string = self.keyset.readline().strip()

            return _string

        return ''.join([choice(KEYSET[self.keyset]) for e in range(self.linelength)])

    def save(self):

        _User = self.__dict__

        # File based keyset
        if type(self.keyset) == file:
            _User['keyset'] = '{0}:{1}'.format(
                            basename(self.keyset.name), self.keyset.tell())

        this.write_user(_User)

def _except_index(max_index, message):
    try:
        _input = raw_input(color(message, 'OKBLUE'))
        assert match(r'^[0-9]+$', _input), 'Invalid user id'

        _input = int(_input)
        assert _input < max_index, 'Invalid user'

    except Exception as e:
        print(color('\n{0}\n', 'WARNING').format(e.message))
        _input = _except_index(max_index, message)
    except KeyboardInterrupt:
        exit()

    return _input

def select_user(newuser, keyset, linelength):
    _user = {
        'name': 'Batman',
        'age': 78,
        'keys':0,
        'time':1,
        'strings':0,
        'keyset':keyset,
        'linelength':linelength
    }
    if newuser:
        _user['name'] = raw_input('Name: ')
        _user['age'] = raw_input('Age: ')
    elif isfile('db'):
        _users = []
        print(color('\nSelect user from db', 'HEADER'))
        with open('db', 'r') as f:
            _temp_id = 0
            for __user in f.readlines():
                if not __user.startswith(';'):
                    _users.append(__user.split('|'))
                    print('{0}. {1}({2} years)'.format(
                        _temp_id,
                        color(_users[-1][0], 'BOLD'),
                        _users[-1][1])
                        )
                    _temp_id += 1

        if _temp_id == 1:
            _selected = _users[0]
        else:
            _selected = _users[_except_index(_temp_id, 'Select user by id (ex 2): ')]

        _user['name'] = _selected[0]
        _user['age'] = _selected[1]

    return _user

'''DATA
User|Age|Keys|Time|Strings|Keyset|LineLength
Batman|76|1395|1255.90067148|93
'''
